# Flatpak developer environment tool
Flatpak has the low level tool called *flatpak*, and a helper for distributing
and building apps called *flatpak-builder*.
However it's missing a developer tool to develop your application from a
flatpak manifest.

This repository is a set of python and bash scripts to cover this case

# Usage
```bash
source setup-flatpak-dev.sh $source-path $manifest-path
```
Where:
 * source-path is where your application code is.
 * manifest-path is where the flatpak manifest to build your application is
   located. Usually it follows a name similar to org.gnome.Nautilus.json

This will download the required SDK and build your application for first
time. Then it will enter the flatpak build environment where you can freely use
your build system tool to build your app and run it inside the flatpak
environment.
You can run the app by just putting the name, and even run under gdb.
For developing for example Nautilus, you would do:
```bash
git clone git@gitlab.com:csoriano/flatpak-dev-cli.git
git clone --recursive https://git.gnome.org/browse/nautilus
source flatpak-dev-cli/setup-flatpak-dev.sh nautilus nautilus/org.gnome.Nautilus.json
nautilus #or gdb nautilus if you want a debugger
```

And wait until everything is installed and you are inside Flatpak environment.

