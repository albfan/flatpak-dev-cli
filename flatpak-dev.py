#!/usr/bin/env python3
# Copyright (c) 2016, Thibault Saunier <tsaunier@gnome.org>
# Copyright (c) 2017, Carlos Soriano <csoriano@gnome.org>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
# Boston, MA 02110-1301, USA.
# pylint: disable=missing-docstring,invalid-name

import argparse
import configparser
import json
import os
import shutil
import subprocess
import sys
import tempfile
import venv
import re
import fileinput

from urllib.parse import urlparse
from urllib.request import urlretrieve
from enum import Enum

FLATPAK_REQ = "0.9.2"

scriptdir = os.path.abspath(os.path.dirname(__file__))

class Colors(Enum):
    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    ENDC = "\033[0m"


class Console:  # pylint: disable=too-few-public-methods

    quiet = False

    @classmethod
    def message(cls, str_format, *args):
        if cls.quiet:
            return

        if args:
            print(str_format % args)
        else:
            print(str_format)

        # Flush so that messages are printed at the right time
        # as we use many subprocesses.
        sys.stdout.flush()


def expand_json_file(manifest_path, outfile, app_path):
    """Creates the manifest file."""
    try:
        os.remove(outfile)
    except FileNotFoundError:
        pass

    with open(manifest_path, "r") as mr:
        contents = mr.read()
        contents = remove_comments(contents)
        json_manifest = json.loads(contents)

    i = 0
    for module in json_manifest["modules"]:
        submanifest_path = None
        if type(module) is str:
            submanifest_path = os.path.join(os.path.dirname(manifest_path), module)
            with open(submanifest_path, "r") as submanifest:
                subcontents = submanifest.read()
                subcontents = remove_comments(subcontents)
                module = json.loads(subcontents)
                json_manifest["modules"][i] = module
        if module["sources"][0]["type"] == "git":
            if os.path.basename(app_path) == module["name"]:
                repo = "file://" + app_path
                module["sources"][0]["url"] = repo

        for source in module["sources"]:
            if source["type"] == "patch" or source["type"] == "file":
                if(submanifest_path is not None):
                    source["path"] = os.path.join(os.path.dirname(submanifest_path), source["path"])
                elif "path" in source:
                    source["path"] = os.path.join(os.path.dirname(manifest_path), source["path"])
        i += 1

    with open(outfile, "w") as of:
        print(json.dumps(json_manifest, indent=4), file=of)


class FlatpakObject:  # pylint: disable=too-few-public-methods

    def __init__(self, user):
        self.user = user

    def flatpak(self, command, *args, show_output=False, comment=None):
        if comment:
            Console.message(comment)

        command = ["flatpak", command]
        if self.user:
            res = subprocess.check_output(command + ["--help"]).decode("utf-8")
            if "--user" in res:
                command.append("--user")
        command.extend(args)

        if not show_output:
            return subprocess.check_output(command).decode("utf-8")

        return subprocess.check_call(command)


class FlatpakPackages(FlatpakObject):  # pylint: disable=too-few-public-methods

    def __init__(self, repos, user=True):
        FlatpakObject.__init__(self, user=user)

        self.repos = repos

        self.runtimes = self.__detect_runtimes()
        self.apps = self.__detect_apps()
        self.packages = self.runtimes + self.apps

    def __detect_packages(self, *args):
        packs = []
        package_defs = [rd for rd in
                        self.flatpak("list", "-d", *args).split("\n") if rd]
        for package_def in package_defs:
            splited_packaged_def = [p for p in package_def.split(" ") if p]
            name, arch, branch = splited_packaged_def[0].split("/")

            # If installed from a file, the package is in no repo
            repo = self.repos.repos.get(splited_packaged_def[1])

            packs.append(FlatpakPackage(name, branch, repo, arch))

        return packs

    def __detect_runtimes(self):
        return self.__detect_packages("--runtime")

    def __detect_apps(self):
        return self.__detect_packages()

    def __iter__(self):
        for package in self.packages:
            yield package


class FlatpakRepos(FlatpakObject):

    def __init__(self, user=True):
        FlatpakObject.__init__(self, user=user)
        self.repos = {}
        self.update()

    def update(self):
        self.repos = {}
        repo_defs = [rd for rd in
                     self.flatpak("remote-list", "-d").split("\n") if rd]
        for repo in repo_defs:
            components = repo.split("	")
            name = components[0]
            desc = ""
            url = None
            for elem in components[1:]:
                if not elem:
                    continue
                parsed_url = urlparse(elem)
                if parsed_url.scheme:
                    url = elem
                    break

                if desc:
                    desc += " "
                desc += elem

            if not url:
                Console.message("No valid URI found for: %s", repo)
                continue

            self.repos[name] = FlatpakRepo(name, desc, url, repos=self)

        self.packages = FlatpakPackages(self)

    def add(self, repo, override=True):
        same_name = None
        for name, tmprepo in self.repos.items():
            if repo.url == tmprepo.url:
                return tmprepo
            elif repo.name == name:
                same_name = tmprepo

        if same_name:
            if override:
                self.flatpak("remote-modify", repo.name, "--url=" + repo.url,
                             comment="Setting repo %s URL from %s to %s"
                             % (repo.name, same_name.url, repo.url))
                same_name.url = repo.url

                return same_name
            else:
                return None
        else:
            self.flatpak("remote-add", repo.name, "--from",
                         repo.repo_file.name,
                         comment="Adding repo %s" % repo.name)

        repo.repos = self
        return repo


class FlatpakRepo(FlatpakObject):  # pylint: disable=too-few-public-methods

    def __init__(self, name, desc=None, url=None,  # pylint: disable=too-many-arguments
                 repo_file=None, user=True, repos=None):
        FlatpakObject.__init__(self, user=user)

        self.name = name
        self.url = url
        self.desc = desc
        self.repo_file_name = repo_file
        self._repo_file = None
        self.repos = repos
        assert name
        if repo_file and not url:
            repo = configparser.ConfigParser()
            repo.read(self.repo_file.name)
            self.url = repo["Flatpak Repo"]["Url"]
        else:
            assert url

    @property
    def repo_file(self):
        if self._repo_file:
            return self._repo_file

        assert self.repo_file_name
        self._repo_file = tempfile.NamedTemporaryFile(mode="w")
        urlretrieve(self.repo_file_name, self._repo_file.name)

        return self._repo_file


class FlatpakPackage(FlatpakObject):
    """A flatpak app."""

    def __init__(self, name, branch, repo, arch, user=True):  # pylint: disable=too-many-arguments
        FlatpakObject.__init__(self, user=user)

        self.name = name
        self.branch = branch
        self.repo = repo
        self.arch = arch

    def __str__(self):
        return "%s/%s/%s %s" % (self.name, self.arch, self.branch, self.repo.name)

    def is_installed(self):
        if not self.repo:
            # Bundle installed from file
            return True

        self.repo.repos.update()
        for package in self.repo.repos.packages:
            if package.name == self.name and \
                    package.branch == self.branch and \
                    package.arch == self.arch:
                return True

        return False

    def install(self):
        if not self.repo:
            return False

        self.flatpak("install", self.repo.name, self.name,
                     self.branch, show_output=True,
                     comment="Installing from " + self.repo.name + " " + \
                             self.name + " " + self.arch + " " + self.branch)

    def update(self):
        if not self.is_installed():
            return self.install()

        self.flatpak("update", self.name, self.branch, show_output=True,
                     comment="Updating %s" % self.name)

    def run_app(self, *args):
        """Starts the app represented by this instance."""
        self.flatpak("run", "--branch=" + self.branch, self.name, *args,
                     show_output=True,
                     comment="Running %s (%s)" % (self.name, self.branch))


class FlatpakModule(FlatpakObject):

    def __init__(self, flatpak_app, manifest, module_name):
        self.flatpak_app = flatpak_app

        with open(manifest, "r") as mr:
            json_manifest = json.load(mr)
        for module in json_manifest["modules"]:
            if module["name"] == module_name:
                break
        self.source_path = module["sources"][0]["url"]
        print (self.source_path)
        print (urlparse(self.source_path))
        print (urlparse(self.source_path).scheme)
        assert urlparse(self.source_path).scheme == "file"
        self.source_path = urlparse (self.source_path).path
        self.build_system = module.get("buildsystem", "autotools")
        self.config_options = module.get("config-opts", [])
        self.make_args = module.get("make-args", [])
        self.make_install_args = module.get("make-install-args", [])
        self.use_builddir = module.get("builddir", "false")

    def configure(self):
        if self.build_system == "meson":
            configure_args = ["meson","build"] + self.config_options + \
                             ["--prefix=/app", "--libdir=/app/lib"]
        elif self.build_system == "autotools" or self.build_system == None:
            configure_args = ["./autogen.sh"] + self.config_options + \
                             ["--prefix=/app", "--libdir=/app/lib"]

        self.flatpak_app.run_in_sandbox(*configure_args, exit_on_failure=True,
                                        cwd=self.source_path)

    def compile(self):
        if self.build_system == "meson":
            compile_args = ["ninja","-C","build"] + self.make_args
        elif self.build_system == "autotools" or self.build_system == None:
            compile_args = ["make"] + self.make_args

        self.flatpak_app.run_in_sandbox(*compile_args, exit_on_failure=True,
                                        cwd=self.source_path)

    def install(self):
        if self.build_system == "meson":
            install_args = ["ninja","-C","build","install"] + self.make_install_args
        elif self.build_system == "autotools" or self.build_system == None:
            install_args = ["make", "install"] + self.make_install_args

        self.flatpak_app.run_in_sandbox(*install_args, exit_on_failure=True,
                                        cwd=self.source_path)


class FlatpakDev:  # pylint: disable=too-many-instance-attributes

    def __init__(self):
        self.sdk_repo = None
        self.runtime = None
        self.locale = None
        self.sdk = None
        self.app = None

        self.packs = []
        self.update = False
        self.devel = False
        self.args = []
        self.build = False
        self.finish_args = None
        self.manifest_path = os.environ.get("FLATPAK_MANIFEST_PATH")
        self.app_path = os.environ.get("FLATPAK_APP_PATH")
        self.name = os.path.basename(self.app_path)
        self.dbus_id = self.__strip_dbus_id(self.manifest_path)
        self.build_name = self.dbus_id + "-generated"
        self.cache_path = os.path.expanduser("~/.cache/flatpak-dev/%s" % self.dbus_id.lower())

        self.prefix = os.path.join(self.cache_path, self.dbus_id.lower())
        self.build_path = os.path.join(self.prefix, "build")

        self.app_module = None

    def __strip_dbus_id (self, manifest_path):
        split_path = manifest_path.split("/")
        manifest_name = split_path[len(split_path) - 1].replace(".json", "")

        return manifest_name

    def check_flatpak(self):
        try:
            output = subprocess.check_output(["flatpak", "--version"])
        except FileNotFoundError:
            Console.message("\n%sYou need to install flatpak >= %s"
                            " to be able to use the '%s' script.\n\n"
                            "You can find some informations about"
                            " how to install it for your distribution at:\n"
                            "    * http://flatpak.org/%s\n", Colors.FAIL,
                            FLATPAK_REQ, sys.argv[0], Colors.ENDC)
            self.exit(1)

        def comparable_version(version):
            return [int(number) for number in version.split(".")]

        version = output.decode("utf-8").split(" ")[1].strip("\n")
        if comparable_version(version) < comparable_version(FLATPAK_REQ):
            Console.message("\n%sFlatpak %s required but %s found."
                            " Please update and try again%s\n", Colors.FAIL,
                            FLATPAK_REQ, version, Colors.ENDC)
            self.exit(1)

    def exit(self, exitcode):
        if self.installer:
            input("Failure installing %s press <enter> to continue" % self.name)

        exit(exitcode)

    def clean_args(self):
        Console.quiet = self.quiet

        self.check_flatpak()

        repos = FlatpakRepos()
        self.sdk_repo = repos.add(
            FlatpakRepo("gnome-nightly",
                        url="http://sdk.gnome.org/nightly/repo/",
                        repo_file="https://sdk.gnome.org/gnome-nightly.flatpakrepo"), False)

        with open(self.manifest_path, "r") as mr:
            contents = mr.read()
            contents = remove_comments(contents)
            json_manifest = json.loads(contents)

        sdk_branch = json_manifest["runtime-version"]
        self.finish_args = json_manifest.get("finish-args", []);
        self.finish_args = remove_extension_points(self.finish_args)
        self.dbus_id = json_manifest["app-id"]
        self.runtime = FlatpakPackage("org.gnome.Platform", sdk_branch,
                                      self.sdk_repo, "x86_64")
        self.locale = FlatpakPackage("org.gnome.Platform.Locale",
                                     sdk_branch, self.sdk_repo, "x86_64")
        self.sdk = FlatpakPackage("org.gnome.Sdk", sdk_branch,
                                  self.sdk_repo, "x86_64")
        self.app = FlatpakPackage(self.dbus_id, "master", None, "x86_64")
        self.packs = [self.runtime, self.locale]

        if self.devel:
            self.packs.append(self.sdk)
        else:
            self.packs.append(self.app)

        self.manifest_generated_path = os.path.join(self.prefix,
                                                    self.build_name + ".json")

    def run_in_sandbox(self, *args, exit_on_failure=False, cwd=None):
        flatpak_command = ["flatpak-builder", "--run"] + self.finish_args +\
                          [self.build_path, self.manifest_generated_path]

        if args:
            flatpak_command.extend(args)
        else:
            flatpak_command.append(os.path.join(scriptdir, "enter-env.sh"))

        Console.message("Running in sandbox: " + ' '.join(flatpak_command))
        try:
            subprocess.check_call(flatpak_command, cwd=cwd)
        except subprocess.CalledProcessError as e:
            if exit_on_failure:
                exit(e.returncode)

    def run(self):
        if self.clean and os.path.exists(self.build_path):
            shutil.rmtree(self.build_path)

        if self.update:
            self.update_all()

        if self.devel:
            if os.path.exists(self.cache_path):
                shutil.rmtree(self.cache_path)
            self.setup_dev_env()

        if not self.devel:
            self.install_all()
            self.app.run_app(*self.args)

    def setup_dev_env(self):
        self.install_all()

        if os.path.exists(self.prefix) and self.update:
            Console.message("Removing prefix %s", self.prefix)
            shutil.rmtree(self.prefix)

        if not os.path.exists(self.build_path):
            Console.message("Building %s %s and dependencies in %s",
                            self.name, self.branch, self.prefix)

            # Create environment dirs if necessary
            os.makedirs(os.path.dirname(self.manifest_generated_path))
            expand_json_file(self.manifest_path, self.manifest_generated_path,
                             self.app_path)
            builder_args = ["flatpak-builder",
                            "--ccache", self.build_path, self.manifest_generated_path]
            builder_args.append("--build-only")
            builder_args.append("--stop-at=%s" % self.name)

            try:
                subprocess.check_call(["flatpak-builder", "--version"])
            except FileNotFoundError:
                Console.message("\n%sYou need to install flatpak-builder%s\n",
                                Colors.FAIL, Colors.ENDC)
                self.exit(1)
            subprocess.check_call(builder_args)

            self.app_module = FlatpakModule(self, self.manifest_generated_path, self.name)
            self.app_module.configure()
            self.app_module.compile()
            self.app_module.install()
        else:
            Console.message("Using %s prefix in %s", self.name, self.prefix)

        if not self.update:
            self.run_in_sandbox(*self.args, exit_on_failure=True)

    def install_all(self):
        for m in self.packs:
            if not m.is_installed():
                m.install()

    def update_all(self):
        for m in self.packs:
            m.update()

def remove_extension_points(array):
    result_args = []
    for arg in array:
        if(not arg.startswith('--extension')):
            result_args.append(arg)
    return result_args

def remove_comments(string):
    pattern = r"(\".*?\"|\'.*?\')|(/\*.*?\*/|//[^\r\n]*$)"
    # first group captures quoted strings (double or single)
    # second group captures comments (//single-line or /* multi-line */)
    regex = re.compile(pattern, re.MULTILINE|re.DOTALL)
    def _replacer(match):
        # if the 2nd group (capturing comments) is not None,
        # it means we have captured a non-quoted (real) comment string.
        if match.group(2) is not None:
            return "" # so we will return empty to remove the comment
        else: # otherwise, we will return the 1st group
            return match.group(1) # captured quoted-string
    return regex.sub(_replacer, string)

if __name__ == "__main__":
    flatpak_dev = FlatpakDev()

    parser = argparse.ArgumentParser(prog="flatpak-dev")

    general = parser.add_argument_group("General")
    general.add_argument("--update", dest="update",
                         action="store_true",
                         help="Update the runtime/sdk/app and rebuild the development environment if needed")
    general.add_argument("--installer", dest="installer",
                         action="store_true",
                         help="Wait for Enter to be pressed when the script exits because something failed")
    general.add_argument("-q", "--quiet", dest="quiet",
                         action="store_true",
                         help="Do not print anything")
    general.add_argument("args",
                         nargs=argparse.REMAINDER,
                         help="Arguments passed when starting %s or, if -d is "
                              "passed, the command to run" % flatpak_dev.name)

    devel = parser.add_argument_group("Development")
    devel.add_argument("-d", "--devel", dest="devel",
                       action="store_true",
                       help="Setup a devel environment")

    devel.add_argument("--branch", dest="branch",
                       help="The flatpak branch to use (stable, master...)",
                       default="master")
    devel.add_argument("-c", "--clean", dest="clean",
                       action="store_true",
                       help="Clean previous builds and restart from scratch")

    parser.parse_args(namespace=flatpak_dev)
    flatpak_dev.clean_args()
    flatpak_dev.run()
